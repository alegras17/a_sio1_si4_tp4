package exemples;

public class Programme5 {
    
    public static void main(String [] args){
    
       int[] tableau=  { 45,23,33,8}; 
         
       System.out.println("\nON AFFICHE TOUS LES ELEMENTS DU TABLEAU DANS L'ORDRE INITIAL\n");
      
       // Utilisation d'un boucle sans indice pour changer ! comme vu dans un exemple prédédent  
       for( int x : tableau)System.out.println(x);
       
       System.out.println("\nON AFFICHE TOUS LES ELEMENTS DU TABLEAU EN PARTANT DU DERNIER JUSQU'AU PREMIER\n");
       
       // Affichage des elements du tableau
       // dans l'ordre inverse de l'ordre initial
       
       // Au premier tour de boucle l'indice i vaut tableau.length-1
       // c'est à dire le dernier comme vu dans un précédent exemple
       // Après avoir exécuté un tour de boucle i est diminué de 1 ( instruction i-- )
       // Une nouvelle boucle sera exécutée si i est supérieur ou égale à ZERO ( condition i>=0 )
       for( int i=tableau.length-1; i>=0; i--) {
       
           // on affiche l'élément de rang i  c'est à dire tableau[i] 
           System.out.println(tableau[i]);
       } 
       
       System.out.println("\n\nON AFFICHE UN ELEMENT SUR 2 DANS L'ORDRE INITIAL\n");
       

       // Dans cette boucle on affiche une élément sur 2
       // puisqua après  chaque tour de boucle i est augmenté de 2 ( instruction i+=2 ) 
       for (int i=0; i<=tableau.length-1; i+=2){
          
           // on affiche l'élément de rang i  c'est à dire tableau[i] 
           System.out.println(tableau[i]);
       }
        System.out.println();
    }
}


