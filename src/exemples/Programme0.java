package exemples;

public class Programme0 {
    
   public static void main(String[] args) {
    
     // boucle pour -->  pour i variant de 1 à 2O refaire une action
   	 // Dans cette boucle la valeur de départ de i est égale à 1 ( intruction int i=1 ) 
   	 // Une boucle n'est ré-exécutée que si i est plus petit ou égale à 20 ( condition i<=20 ) 
     // A chaque tour de boucle i augmente de 1 (   instruction i++  )  
     for ( int i=1 ; i<=20; i++){
     
         // On affiche le n° de tour  c'est à dire le contenu de la variable i 
         System.out.println("Tour N° "+ i);     
     }          
   }   
}


