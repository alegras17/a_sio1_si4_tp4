package exemples;
import java.util.Scanner;

public class Programme7 {
    
    public static void main(String[] args){
        
       Scanner  clavier = new Scanner(System.in);
       int    indiceRecherche; 
       
       
       // Déclaration d'un tableau de chaines de caractères  
       // c'est la notation String[] qui indique qu'il s'agit d'un tableau de chaine de caratères 
       String[] tableau = { "Sophie","Alain","Jacques","Béatrice" };
        
       // Affichage des éléments du tableau
       for( int indice=0; indice<= tableau.length-1; indice++ ) {
       
           System.out.println("indice: "+indice+"  valeur: "+tableau[indice]);
       } 
       
       System.out.println("\n\n");
       
       
       // Saisie de l'indice de l'élément à afficher
       System.out.println("Indiquez l'indice de l'élément recherché (entre 0 et 3)");
       indiceRecherche=clavier.nextInt(); 
             
       // Affichage de l'indice saisi et de l'élément qui correspond
       System.out.println("\nL'élément d'indice "+indiceRecherche+ " est "+tableau[indiceRecherche]);
       System.out.println();
        
    }    
}






