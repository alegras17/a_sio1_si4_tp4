package exemples;

import java.util.Scanner;

public class Programme6 {
    
    public static void main(String[] args){

       // Déclaration du clavier
       Scanner  clavier= new Scanner(System.in); 
        
       // Déclaration et initialisation d'un tableau de 4 entiers 
       int[] tableau=  { 45,23,33,8};
    
       // affichage des elements du tableau
       for(int x : tableau) System.out.printf("%3d",x);
       
       System.out.println("\n");
       
       int   indice;
    
       // Saisie de l'indice de l'element à afficher
       System.out.println("Indice de l'élément à afficher? ");
       indice=clavier.nextInt();
     
       // Accès à un element du tableau en utilisant de son indice
       

       // Affichage formaté 
       // %2d sera remplacé par la valeur de indice sur 2 positions
       // %3d sera remplace par la valeur de tableau[indice]  sur 3 positions
       
       System.out.printf
        ("\nL'élément à l'indice %2d est %3d\n\n",
                   indice, tableau[indice]);           
    }  
}
