package exemples;

import java.util.Random;
import java.util.Scanner;

public class Programme8 {
    
    public static void main(String[] args){
         
        // On déclare un gégrateur de nombres aléatoires        
        Random   genAleat  = new Random();
        // On déclare le clavier
        Scanner  clavier   = new Scanner(System.in);
    
        // Déclaration d'un titi de 10 entiers 
        int[] titi=  new int[10];
    
        int   indiceRecherche;
      
        // remplissage d'un titi avec des nombres aléatoires entre 1 et 10000
        for(int indice=0; indice<titi.length; indice++){
        
            titi[indice]=genAleat.nextInt(1000)+1;
        }
          
        //Affichage des éléments du titi
        for (int i=0; i<titi.length;i++){
        
            System.out.printf("indice %2d valeur: %3d\n",
                              i,titi[i]);
        }
        
        // Saisie de l'indice de lélément recherché
        System.out.println("Indice de l'élément à afficher? ");
        indiceRecherche=clavier.nextInt();
        
        // Affichage de l'indice saisi et de l'élément qui correspond
        System.out.println(
       "L'élément d'indice "+
  indiceRecherche+" est "+ titi[indiceRecherche]);           
    }   
}
