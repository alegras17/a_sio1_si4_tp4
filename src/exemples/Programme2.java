package exemples;

import java.util.Scanner;

public class Programme2 {
    
    public static void main(String[] args) {
    
       Scanner clavier= new Scanner(System.in); 
        
        
       // Déclaration et initialisation du tableau  
       // A l'aide d'une boucle de saisie 
        
       // On déclare un tableau qui contiendra des entiers  et on reserve 5 places dans celui-ci
       int[] tableau= new int[5] ; 
       
       System.out.println("\nSAISIE DES ELEMENTS DU TABLEAU\n");
       
       // cf explication exemples précédents
       for( int indice=0; indice <= tableau.length-1; indice++ ) {
           
           // On saisit un entier grâce à l'instruction clavier.nextInt()
           // et on le range dans le poste de table de rang: indice    (   tableau[indice]   )       
           System.out.println("Entrez un nombre entier?");
           tableau[indice] = clavier.nextInt();
       }   
       
       System.out.println("\nAFFICHAGE DES ELEMENTS DU TABLEAU\n");
        
       // Affichage des éléments du tableau en utilisant un indice
       // Attention le premier indice d'un tableau est l'indice ZERO
       
       for( int indice=0; indice <= tableau.length-1 ; indice++ ) {
           

           // on affiche l'élément du tableau de rang: indice        
           System.out.println(tableau[indice]);
       }               
    }   
}


