package exemples;

public class Programme1 {
    
    public static void main(String[] args) {
    
       //  Déclaration et initialisation du tableau 
       //  int [] signifie tableau d'entiers  le fait que c'est un tableau est indiqué par []
       //  Les valeurs enregistrées dans le tableau sont enumérées entre le accolades  
       int[] tableau = { 45, 23, 33,66,89,45,
                         76,32,56,77,
                         55,56,89,67,45,99 }; 
       
       // Affichage des éléments du tableau en utilisant un indice
       // Attention le premier indice d'un tableau est l'indice ZERO
       // tableau.length donne le nombre d'lément du tableau c'est à dire  15 ici
       for( int indice=0; indice <= tableau.length-1 ; indice++ ) {
             

           // tableau[indice] dans l'instruction qui suit permet 
           // d'obtenir l'élément de rang indice  du tableau pour l'afficher
           // NB: A chaque tour de boucle indice prend a nouvelle valeur
           // puisque  à chaque nouveau tour de boucle l'instruction indice++ est exécutée  
                  
           System.out.println(tableau[indice]);
       }             
       
    }   
}


