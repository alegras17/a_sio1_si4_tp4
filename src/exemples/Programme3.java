package exemples;

import java.util.Arrays;

public class Programme3 {
    
    public static void main(String[] args) {
    
       
       // Déclaration et initialisation d'un tableau avec 15 entiers  
        
       int[] tableau={12,7,65,14,83,33,29,75,28,17,65,24,52,33,1}; 
       
       System.out.println("\nAFFICHAGE DES ELEMENTS DU TABLEAU DANS L'ORDRE INITIAL\n");
       
       for( int indice=0; indice <= tableau.length-1 ; indice++ ) {
                  
           System.out.printf("%3d\n",tableau[indice]);
       }             
       
        System.out.println();
       
      // Tri du tableau avec la fonction ARRAYS.sort
         
       Arrays.sort(tableau);
     
       System.out.println("\nAFFICHAGE DES ELEMENTS DU TABLEAU APRES LES AVOIR TRIES EN ORDRE CROISSANT\n");
        
       // Affichage des éléments du tableau en utilisant un indice
       // Attention le premier indice d'un tableau est l'indice ZERO
       // tableau.length donne le nombre d'éléments du tableau
       // tableau.length-1 donne le dernier indice du tableau
       // puisque le premier indice est ZERO
       // exemple: un tableau de 3 éléments  comporte les indice 0 1 et 2 ( c'est à dire 3-1)
       
       for( int indice=0; indice <= tableau.length-1 ; indice++ ) {
               
           
           System.out.printf("%3d ",tableau[indice]);
       }             
       
       System.out.println("\n");
       
    }   
}


