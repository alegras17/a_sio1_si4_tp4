package exemples;

public class Programme4 {
                
    public static void main(String [] args){    
       
        // Parcours et affichage des éléments du tableau sans utiliser d'indice 
       
        // Déclaration et initailisation d'un tableau de 4 entiers     
        int[] tableau = { 45, 23, 33, 8 };
        
        // Boucle forEach
        // Pour chaque élément  x de type entier(int) dans le  tableau
        // Le symbole : signifie dans 
        // Dans  cette boucle on utilise pas d'indice 
        for( int x : tableau ){
       
           // a chaque tour de boucle on affiche x 
           System.out.println(x);
        }
    }    
}
