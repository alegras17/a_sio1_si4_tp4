package exercices;

public class TP04Ex03 {

    public static void main(String[] args) {
        
          float[] notes = { 12, 8, 14, 7, 9.5f, 14, 15.5f, 6, 16, 11, 14, 10.5f };
 
          float moyenne;
          float noteMin=100;
          float noteMax=0;
          float totalNotes = 0;
          
          for( int indice=0; indice <= notes.length-1 ; indice++ ){
              
               totalNotes = totalNotes + notes[indice];
               
               if (notes[indice]<noteMin){
                   
                   noteMin = notes[indice];
                   
               }
              
               if (notes[indice]>noteMax){
                   
                   noteMax=notes[indice];
                   
               }
          } 
          
          moyenne = totalNotes / (notes.length-1);
          
          System.out.println("Moyenne : " + moyenne);
          System.out.println("Note mini : " + noteMin);
          System.out.println("Note maxi : " + noteMax);
    } 
}


